import React from 'react';
import ReactDOM from 'react-dom';
import NrmHub from "../../assets/js/react/components/NrmHub";

export default function displayHub() {
  const hubBlocks = document.querySelectorAll('[data-hub]');
  for (var i in hubBlocks) if (hubBlocks.hasOwnProperty(i)) {
    ReactDOM.render(React.createElement(NrmHub), document.querySelector('#'+hubBlocks[i].getAttribute("id")));
  }
} 