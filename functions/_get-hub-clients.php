<?php
function get_hub_clients( $type = '' ) {
  $aplication_password = new Application_Passwords();
  $hub_clients = [];
  foreach ($aplication_password->get_user_application_passwords(2) as $client) {
    $name = $client["name"];
    $slug = 'nrhub_'.$client["name"];
    switch( $type ) {
      case 'name':
        $hub_clients[] = $name;
        break;
      case 'slug':
        $hub_clients[] = $slug;
        break;
      default:
        $client_data_structure = [
          'name' => $name, 
          'slug' => $slug
        ];
        $hub_clients[] = (object)$client_data_structure;
    }
  }
  return $hub_clients;
}
