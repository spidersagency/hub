<?php
function register_block_rest_route($data) {
  $hub_page_id = 5; //TODO zmienic id 5 (aktualny home) na zmienna
  $hub_page_blocks = parse_acf_blocks(get_post_field('post_content', $hub_page_id));
  $block_id = $data['id'];
  foreach ($hub_page_blocks as $block) {
    if ($block_id == $block['attrs']['id']) {
      if($block['attrs']['data']) {
        $block_fields = $block['attrs']['data'];
      } else {
        $block_fields = [];
      }
    }
  }
  $post_ids = get_field($block_id, $hub_page_id); 
  $block_posts = [];
  $block_posts['fields'] = $block_fields;
  if(is_array($post_ids)) {
    foreach($post_ids as $post_id) {
      $block_posts['posts'][] = api_post_structure($post_id);
    }
  } else {
    $block_posts['posts'] = [];
  }

  return $block_posts;
}



add_action( 'rest_api_init', function () {
  register_rest_route( 'hub/v2', '/(?P<id>\w+)', array(
    'methods' => 'GET',
    'callback' => 'register_block_rest_route',
  ) );
} );
