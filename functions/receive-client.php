<?php
function post_exist($nrhub_post_id) {
  global $wpdb;
  $find_exist_post = $wpdb->get_results( "select * from $wpdb->postmeta where meta_key = 'nrhub_post_id' and  meta_value = '".$nrhub_post_id."'" );
  if ($find_exist_post) {
    $exist_post_id = $find_exist_post[0]->post_id;
  } else {
    $exist_post_id = null;
  }
  return $exist_post_id;
}

function upload_image($img_url, $post_id) {
  $image_url = $img_url;
  $upload_dir = wp_upload_dir();
  $image_data = file_get_contents( $image_url );
  $filename = basename( $image_url );
  if ( wp_mkdir_p( $upload_dir['path'] ) ) {
    $file = $upload_dir['path'] . '/' . $filename;
  }
  else {
    $file = $upload_dir['basedir'] . '/' . $filename;
  }
  file_put_contents( $file, $image_data );
  $wp_filetype = wp_check_filetype( $filename, null );
  $attachment = array(
    'post_mime_type' => $wp_filetype['type'],
    'post_title' => sanitize_file_name( $filename ),
    'post_content' => '',
    'post_status' => 'inherit'
  );
  $attach_id = wp_insert_attachment( $attachment, $file );
  require_once( ABSPATH . 'wp-admin/includes/image.php' );
  $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
  wp_update_attachment_metadata( $attach_id, $attach_data );
  update_post_meta( $post_id, '_thumbnail_id', $attach_id );
}

function nrhub_create_user($email_address, $name, $post_id) {
  if( null == username_exists( $email_address ) ) {
    $password = wp_generate_password( 12, false );
    $user_id = wp_create_user( $email_address, $password, $email_address );
    wp_update_user(
      array(
        'ID' => $user_id,
        'nickname' => $name
      )
    );
    $user = new WP_User( $user_id );
    $user->set_role( 'author' );
  }
  $arg = array(
    'ID' => $post_id,
    'post_author' => get_user_by('email', $email_address)->ID,
  );
  wp_update_post( $arg );
}

foreach (get_hub_clients() as $client) {
  add_action("rest_insert_".$client->slug, "update_post_after_insert_from_hub_client",  10, 3);
}
function update_post_after_insert_from_hub_client(\WP_Post $post, $request, $creating) {
  /**
   * Dodanie przyjmowania meta po REST-API
   */
  $metas = $request->get_param("meta");

  if ( post_exist($metas['nrhub_post_id']) ) {
    $post_id = post_exist($metas['nrhub_post_id']);
    $request_post = array(
      'ID'           => $post_id,
      'post_title'   => $request->get_param("title"),
      'post_content' => $request->get_param("content"),
    );
    wp_update_post( $request_post );
    if (get_field( 'nrhub_thumbnail_url', $post_id ) != $metas['nrhub_thumbnail_url']) {
      upload_image($metas['nrhub_thumbnail_url'], $post_id);
    }
    wp_delete_post($post->ID);
  } else {
    $post_id = $post->ID;
    /**
     * Automatyczna publikacja dla nowego posta
     */
    foreach (get_hub_clients() as $client) {
      if ( $metas['nrhub_client_id'] == $client->name && get_field('auto-publish_'.$client->name, 'option') ) {
        wp_update_post( array( 'ID' => $post_id, 'post_status' => 'publish', ) );
      }
    }
    upload_image($metas['nrhub_thumbnail_url'], $post_id);
  }


  if ( is_array($metas) ) {
    foreach ($metas as $name => $value) {
      update_post_meta($post_id, $name, $value);
    }
  }

  wp_set_object_terms($post_id, $metas['nrhub_tags'], 'nrhub_post_tag');
  wp_set_object_terms($post_id, $metas['nrhub_categories'], 'nrhub_category');

  nrhub_create_user($metas['nrhub_post_author']['e-mail'], $metas['nrhub_post_author']['name'], $post_id);

}
